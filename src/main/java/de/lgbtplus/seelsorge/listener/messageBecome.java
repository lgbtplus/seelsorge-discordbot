package de.lgbtplus.seelsorge.listener;

import de.lgbtplus.seelsorge.Main;
import de.lgbtplus.seelsorge.utils.MessageManager;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

/**
 * Created by xCookiiee on 11.07.2018.
 */

public class messageBecome extends ListenerAdapter {


  public void onMessageReceived(MessageReceivedEvent e) {

    JDA core = e.getJDA();
    Guild guild = core.getGuildById(389458795237081089L); // trackt meinen einen Server
    TextChannel channel = guild.getTextChannelById(445656714839195663L); // trackt den Channel auf dem Server
    TextChannel logChannel = guild.getTextChannelById(445676878901280800L); // trackt den Admin-Log-Channel


    String newName;

    final Message message = e.getMessage();

    if (guild.isMember(message.getAuthor()) && message.getChannelType().equals(ChannelType.PRIVATE) && e.getAuthor() != core.getSelfUser()) {
      if (message.getContentRaw().startsWith("/changeName")) {
        newName = Main.getResolver().createUID(e.getAuthor(), true);
        e.getMessage().getChannel().sendMessage("Dein neuer Name lautet:" + newName).queue();
      }
      else {
        channel.sendMessage(MessageManager.B(message, guild.getIconUrl())).queue();
      }
    } else if (!guild.isMember(message.getAuthor()) && message.getChannelType().equals(ChannelType.PRIVATE)) {
      e.getMessage().getChannel().sendMessage("Nix, da, so läuft das hier aber nicht. lol").queue();
    }

    if (message.getChannel().getIdLong() == logChannel.getIdLong()) {
      if (message.getContentRaw().startsWith("/getuser")) {

        for (Role roles : guild.getMember(message.getAuthor()).getRoles()) {
          if (roles.getName().equals(".")) {
            String[] args = message.getContentRaw().split(" ");
            if (args.length > 1) {
              String id = args[1];
              if (Main.getResolver().getUser(id) == null) {
                logChannel.sendMessage("Der Name **" + id + "** existiert leider nicht.").queue();
                return;
              }
              logChannel.sendMessage("Der Author von der Nachricht mit dem Namen **" + id + "** lautet: " + Main.getResolver().getUser(id).getAsMention()).queue();
            } else {
              logChannel.sendMessage("**Verwende bitte /getuser <Name>**").queue();
            }


            return;
          }
        }
        message.getChannel().sendMessage(message.getAuthor().getAsMention() + " **Du hast leider keine Befugnis den Verfasser zu ermitteln**").queue();
      }
      else if (message.getContentRaw().startsWith("/getolduser")) {

        for (Role roles : guild.getMember(message.getAuthor()).getRoles()) {
          if (roles.getName().equals(".")) {
            String[] args = message.getContentRaw().split(" ");
            if (args.length > 1) {
              String id = args[1];
              if (Main.getResolver().getOldUser(id) == null) {
                logChannel.sendMessage("Der Name **" + id + "** existiert leider nicht.").queue();
                return;
              }
              logChannel.sendMessage("Der Author von der Nachricht vor dem Namenswechsel mit dem Namen **" + id + "** lautet: " + Main.getResolver().getOldUser(id).getAsMention()).queue();
            } else {
              logChannel.sendMessage("**Verwende bitte /getolduser <Name>**").queue();
            }


            return;
          }
        }
        message.getChannel().sendMessage(message.getAuthor().getAsMention() + " **Du hast leider keine Befugnis den Verfasser zu ermitteln**").queue();
      }
    }


  }


}